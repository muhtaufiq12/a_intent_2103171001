package taufiq12.aplikasiintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mExplicitIntentBtn = findViewById(R.id.ExplicitIntentBtn);
        Button mImplicitIntentBtn = findViewById(R.id.ImplicitIntentBtn);

        mExplicitIntentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ExplicitIntent.class));
            }
        });

        mImplicitIntentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getPackageManager().getLaunchIntentForPackage("taufiq12.kalkulator");
                startActivity(intent);
            }
        });
    }


}
